import React, { useEffect, useState } from 'react'
import axios from 'axios'
import TodoCard from '../components/TodoCard'

const URL = 'https://api-nodejs-todolist.herokuapp.com'
const TOKEN =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDQ2MGVmYmIxZDg3NTAwMTczOGQwODQiLCJpYXQiOjE2MTU0NjEwMjd9.cCLbT2beXMPjQ7OAaYlyPZXrFtpvEucGDNN8krtteas'
const axiosConfig = {
  headers: {
    Authorization: `Bearer ${TOKEN}`
  }
}

function TodoList() {
  const [tasks, setTasks] = useState([])
  const [editMode, setEditMode] = useState({ isEdit: false, taskId: '' })
  const [taskInput, setTaskInput] = useState('')
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    ;(async () => await fetchTasks())()
  }, [])

  async function fetchTasks() {
    try {
      setLoading(true)
      const { data } = await axios.get(`${URL}/task`, axiosConfig)
      setTasks(data.data)
    } catch (err) {
      console.error('❌ Error:', err.message)
    } finally {
      setLoading(false)
    }
  }

  function handleDisplayInputEdit(_, taskId) {
    setEditMode({ taskId, isEdit: true })
  }

  async function handleUpdateTaskCompleted(e, taskId, completed) {
    try {
      setLoading(true)
      await axios.put(
        `${URL}/task/${taskId}`,
        { completed: !completed },
        axiosConfig
      )
      await fetchTasks()
    } catch (err) {
      console.error('❌ Error:', err.message)
    } finally {
      setLoading(false)
    }
  }

  async function handleEditTask(e, taskId) {
    if (e.key === 'Enter') {
      try {
        setLoading(true)
        await axios.put(
          `${URL}/task/${taskId}`,
          { description: taskInput },
          axiosConfig
        )
        setEditMode({ isEdit: false, taskId: '' })
        await fetchTasks()
      } catch (err) {
        console.error('❌ Error:', err.message)
      } finally {
        setLoading(false)
      }
    }
  }

  async function handleDeleteTask(e, taskId, taskName) {
    e.stopPropagation()
    const isConfirmed = window.confirm(`คุณจะลบ Task ${taskName} หรือไม่ ?`)
    if (isConfirmed) {
      try {
        setLoading(true)
        await axios.delete(`${URL}/task/${taskId}`, axiosConfig)
        await fetchTasks()
      } catch (err) {
        console.error('❌ Error:', err.message)
      } finally {
        setLoading(false)
      }
    }
  }

  return (
    <div>
      {loading ? (
        <h3>Loading...</h3>
      ) : (
        <>
          <h3>Todo List</h3>
          <h4>Double click on task name to edit</h4>
          <ul
            style={{
              listStyle: 'none',
              display: 'flex',
              flexDirection: 'column',
              gap: 15,
              alignItems: 'center'
            }}
          >
            {tasks.map((task) => {
              return (
                <TodoCard
                  key={task._id}
                  editMode={editMode}
                  todo={task}
                  onDisplayInputEdit={handleDisplayInputEdit}
                  onEditTask={handleEditTask}
                  taskInput={taskInput}
                  onUpdateTodoCompleted={handleUpdateTaskCompleted}
                  onDeleteTask={handleDeleteTask}
                  setTaskInput={setTaskInput}
                />
              )
            })}
          </ul>
        </>
      )}
    </div>
  )
}

export default TodoList
