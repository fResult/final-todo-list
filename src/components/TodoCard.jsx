import React from 'react'
import './TodoCard.css'

function TodoCard({
  editMode,
  todo,
  onDisplayInputEdit,
  onUpdateTaskCompleted,
  onDeleteTask,
  taskInput,
  setTaskInput
}) {
  return (
    <li key={todo._id} className="todo-card">
      <div style={{ textAlign: 'right' }}>
        <button onClick={(e) => onDeleteTask(e, todo._id, todo.description)}>
          ❌
        </button>
      </div>
      <div>
        <div>
          {editMode?.isEdit && editMode.taskId === todo._id ? (
            <div
              style={{
                display: 'inline-block',
                width: 200 - 10,
                textAlign: 'right'
              }}
            >
              <input
                ref={input => input?.select()}
                type="text"
                onKeyUp={(e) => onEditTask(e, todo._id)}
                onChange={(e) => setTaskInput(e.target.value)}
                value={taskInput}
                style={{
                  display: 'inline',
                  width: 200,
                  marginRight: 15
                }}
              />
            </div>
          ) : (
            <span
              onDoubleClick={(e) => {
                onDisplayInputEdit(e, todo._id)
                setTaskInput(todo.description)
              }}
              className="desc-text"
            >
              {todo.description}
            </span>
          )}
          <div>
            <input
              id="completed"
              type="checkbox"
              checked={todo.completed}
              onChange={(e) =>
                onUpdateTaskCompleted(e, todo._id, todo.completed)
              }
            />
            <label htmlFor="completed">Complete?:</label>
          </div>
        </div>
      </div>
    </li>
  )
}

export default TodoCard
